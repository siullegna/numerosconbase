﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NumerosConBase
{
    public partial class NumBase : Form
    {
        private struct sNum_Base
        {
            public string strLabel;
            public int intBase;
            public string strOperacion;
        }
        private string strBase;
        private sNum_Base[] vNum_Base;
        private TreeNode tnNum_b = new TreeNode();  // Nodo Padre
        private TreeNode tnCBase = new TreeNode();  // Nodo de La Base
        //private TreeNode tnNum = new TreeNode();    // Nodo del Numero
        private int intBase;
        private char[] charNum;
        private int intRow;

        public NumBase()
        {
            InitializeComponent();
            mVoidStart();
        }
        private void mVoidStart()
        {
            btnEvaluar.Enabled = false;
            mVoidToolTip();
        }
        private void mVoidToolTip()
        {
            ToolTip ttCompilar = new ToolTip();
            ttCompilar.AutoPopDelay = 500;
            ttCompilar.InitialDelay = 500;
            ttCompilar.ReshowDelay = 500;
            ttCompilar.ShowAlways = true;
            ttCompilar.SetToolTip(this.btnEvaluar, "Ejecutar Operación");
        }
        private void txtNumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            bool flagBase = false;
            if (e.KeyChar != (char)Keys.Back)
            {
                if (txtNumero.Text.IndexOf('O') != -1 || txtNumero.Text.IndexOf('L') != -1 || txtNumero.Text.IndexOf('H') != -1 ||
                    txtNumero.Text.IndexOf('N') != -1)
                    flagBase = true;
                if (mBoolKeys(e.KeyChar) || mBoolBase(e.KeyChar))
                {
                    if (e.KeyChar == '.')
                        e.Handled = true;
                    if (flagBase)
                        e.Handled = true;
                }
                else
                    e.Handled = true;
                if (flagBase && mBoolKeys(e.KeyChar))
                    e.Handled = true;
            }
        }
        private bool mBoolKeys(char e)
        {
            if (e == (char)Keys.Back || e == (char)Keys.Delete || e == (char)Keys.Left || e == (char)Keys.Right || Char.IsNumber(e) || e == 'a' ||
                e == 'b' || e == 'c' || e == 'd' || e == 'e' || e == 'f' || e == 'A' || e == 'B' || e == 'C' || e == 'D' || e == 'E' || e == 'F')
                return true;
            return false;
        }
        private bool mBoolBase(char e)
        {
            if (e == 'N' || e == 'O' || e == 'L' || e == 'H')
                return true;
            return false;
        }
        private void txtNumero_TextChanged(object sender, EventArgs e)
        {
            if (txtNumero.Text.IndexOf('N') != -1 || txtNumero.Text.IndexOf('O') != -1 || txtNumero.Text.IndexOf('L') != -1 ||
                txtNumero.Text.IndexOf('H') != -1)
                btnEvaluar.Enabled = true;
            else
                btnEvaluar.Enabled = false;
        }
        private void btnEvaluar_Click(object sender, EventArgs e)
        {
            try
            {
                char[] CharAux = txtNumero.Text.ToCharArray();
                int contBase = 0;           // Contar las veces que se agregó una base
                bool flagBaseSi = false;    // para que haya por lo menos una base
                bool flagDespues = false;   // para que no haya más numeros después de la base
                bool flagNumero = false;
                for(int i = 0; i < CharAux.Length; i++)
                {
                    if (mBoolBase(CharAux[i]))
                    {
                        contBase++;
                        flagBaseSi = true;
                    }
                    if (mBoolKeys(CharAux[i]))
                        flagNumero = true;
                    if (flagBaseSi && Char.IsNumber(CharAux[i]))
                        flagDespues = true;
                }
                if (!flagDespues && contBase == 1 && flagNumero)
                {
                    this.tvResultado.Nodes.Clear();
                    mTreeView();
                    this.tvResultado.ExpandAll();
                    txtNumero.Text = strBase;
                }
                else
                {
                    MessageBox.Show("Error: El Número No Tiene El Formato Correcto [NumeroBase]", "Número Con Base", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Número Con Base", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////
        private TreeView mTreeView()
        {
            // Método Para Validar La Base
            mVoidValidarBase();
            // Para Llenar "Struct"
            strBase = txtNumero.Text;
            txtNumero.Text = txtNumero.Text.Substring(0, txtNumero.Text.Length - 1);
            intRow = txtNumero.Text.Length * 3 + 3;   // El 3 es por... Num_base, cBase y Base
            vNum_Base = new sNum_Base[intRow];
            // Le coloca a todos los nodos, la base que le corresponde.
            for (int i = 0; i < intRow; i++)
            {
                vNum_Base[i].intBase = intBase;
                vNum_Base[i].strOperacion = string.Empty;
            }
            charNum = txtNumero.Text.ToCharArray();
            mVoidLabels();
            mVoidOperaciones();
            vNum_Base[intRow - 2].strLabel = "cBase";
            vNum_Base[intRow - 2].intBase = intBase;
            vNum_Base[intRow - 2].strOperacion = string.Empty;
            vNum_Base[intRow - 1].intBase = intBase;
            vNum_Base[intRow - 1].strOperacion = string.Empty;
            // Para Llenar "TreeView"
            tnNum_b = new TreeNode(vNum_Base[0].strLabel + " [(" + vNum_Base[0].intBase + "), " + "(" + vNum_Base[0].strOperacion + ")]");
            this.tvResultado.Nodes.Add(tnNum_b);
            // Método Para Colocar La Parte del Número.
            mVoidIzquierda();
            // Método Para Colocar La Base del Número
            mVoidDerecha();
            return this.tvResultado;
        }
        private void mVoidLabels()
        {
            vNum_Base[0].strLabel = "Número Con Base";
            vNum_Base[0].intBase = intBase;
            int intPosX = 0;    // Para comenzar con las derivaciones desde Numero
            for (int i = charNum.Length - 1; i >= 0; i--)
            {
                for (int j = 0; j < 3; j++)
                {
                    intPosX++;
                    switch (j)
                    {
                        case 0:
                            vNum_Base[intPosX].strLabel = "Núm";
                            break;
                        case 1:
                            vNum_Base[intPosX].strLabel = "Dig";
                            break;
                        case 2:
                            vNum_Base[intPosX].strLabel = charNum[i].ToString();
                            break;
                    }
                }
            }
        }
        private void mVoidOperaciones()
        {
            // Va de Izquierda a Derecha: Numero. 348D
            // Va de Abajo para Arriba: Enumeración.
            int intCurrentPos = intRow - 3;
            int intOpe;
            for (int i = 0; i < charNum.Length; i++)
            {
                for (int j = 0; j < 3; j++) // Recorre: strLabel, intBase, strOperación
                {
                    intOpe = mIntDigito(charNum[i]);
                    if (i == 0)  // Se guardará el primer Número, puede ser: N ó Error
                    {
                        if (intOpe == -1)
                            vNum_Base[intCurrentPos].strOperacion = "Error";
                        else
                            vNum_Base[intCurrentPos].strOperacion = intOpe.ToString();
                    }
                    else
                    {   // Ya esta con otro número!
                        if (j == 2) // Ya está en la etiqueta de "Num"
                        {
                            if (vNum_Base[intCurrentPos + 3].strOperacion.IndexOf(",") != -1) // Tiene Números de la forma: N , N
                            {
                                if (intOpe == -1)
                                    vNum_Base[intCurrentPos].strOperacion = "Error";
                                else
                                    vNum_Base[intCurrentPos].strOperacion = intOpe.ToString();
                                string strCurr = vNum_Base[intCurrentPos + 3].strOperacion;
                                // Izquierda ("N", n)
                                string strIzquierda = mStrIzquierda(strCurr);
                                // Derecha (n, "N")
                                string strDerecha = vNum_Base[intCurrentPos + 5].strOperacion;
                                if (!strIzquierda.Equals("Error") && !strDerecha.Equals("Error"))
                                {
                                    if (intOpe != -1)
                                        vNum_Base[intCurrentPos].strOperacion = "(" + (Int32.Parse(strIzquierda) * intBase +
                                            Int32.Parse(strDerecha)).ToString() + ", " + intOpe + ")";
                                    else
                                        vNum_Base[intCurrentPos].strOperacion = "(" + (Int32.Parse(strIzquierda) * intBase +
                                                Int32.Parse(strDerecha)).ToString() + ", Error)";
                                }
                                else
                                {
                                    if (intOpe == -1)
                                        vNum_Base[intCurrentPos].strOperacion = "(Error, Error)";
                                    else
                                        vNum_Base[intCurrentPos].strOperacion = "(Error, " + intOpe + ")";
                                }
                            }
                            else   // Tiene Números de La Forma: N
                            {
                                if (intOpe == -1)
                                    vNum_Base[intCurrentPos].strOperacion = "(" + vNum_Base[intCurrentPos + 3].strOperacion + ", Error)";
                                else
                                    vNum_Base[intCurrentPos].strOperacion = "(" + vNum_Base[intCurrentPos + 3].strOperacion + ", " + intOpe + ")";
                            }
                        }
                        else
                        {
                            if (intOpe == -1)
                                vNum_Base[intCurrentPos].strOperacion = "Error";
                            else
                                vNum_Base[intCurrentPos].strOperacion = intOpe.ToString();
                        }
                    }
                    intCurrentPos--;
                }
            }
            
            if (!mStrIzquierda(vNum_Base[1].strOperacion).Equals("Error") && !vNum_Base[3].strOperacion.Equals("Error"))
            {
                if (txtNumero.Text.Length > 1)
                {
                    txtResultado.Text = (Int32.Parse(mStrIzquierda(vNum_Base[1].strOperacion)) * intBase +
                        Int32.Parse(vNum_Base[3].strOperacion)).ToString();
                    vNum_Base[0].strOperacion = txtResultado.Text;
                }
                else
                {
                    txtResultado.Text = vNum_Base[1].strOperacion;
                    vNum_Base[0].strOperacion = txtResultado.Text;
                }
            }
            else
            {
                txtResultado.Text = "Error";
                vNum_Base[0].strOperacion = txtResultado.Text;
                MessageBox.Show("Error Semántico", "ErroR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private string mStrIzquierda(string strCurr)
        {
            if (strCurr.Length <= 1)
                return strCurr;
            else
            {
                if (strCurr.IndexOf(",") != -1)
                {
                    if (!strCurr.Equals("Error"))
                        return strCurr.Substring(1, strCurr.IndexOf(",") - 1);
                    else
                        return strCurr;
                }
                else
                    return strCurr;
            }
        }
        private void mVoidValidarBase()
        {   // Bases: N -> Binaria, O -> Octal, L -> Décimal & H -> Hexadécimal
            if (txtNumero.Text.IndexOf("N") != -1)
                this.intBase = 2;
            else
            {
                if (txtNumero.Text.IndexOf("O") != -1)
                    this.intBase = 8;
                else
                {
                    if (txtNumero.Text.IndexOf("L") != -1)
                        this.intBase = 10;
                    else
                        this.intBase = 16;
                }
            }
        }
        private void mVoidIzquierda()
        {
            int intPos = intRow - 3;
            TreeNode tnRaiz = null;
            TreeNode tnNum = null;
            TreeNode tnDig = null;
            TreeNode tnVal = null;
            for (int j = 0; j < 3; j++)
            {
                switch (j)
                {
                    case 0: // Valor
                        tnVal = new TreeNode(vNum_Base[intPos].strLabel + " [(" + vNum_Base[intPos].intBase.ToString() + "), " +
                            vNum_Base[intPos].strOperacion + "]");
                        break;
                    case 1: // Digito
                        tnDig = new TreeNode(vNum_Base[intPos].strLabel + " [(" + vNum_Base[intPos].intBase.ToString() + "), " +
                            vNum_Base[intPos].strOperacion + "]");
                        tnDig.Nodes.Add(tnVal);
                        break;
                    case 2: // Numero
                        tnNum = new TreeNode(vNum_Base[intPos].strLabel + " [(" + vNum_Base[intPos].intBase.ToString() + "), " +
                            vNum_Base[intPos].strOperacion + "]");
                        tnNum.Nodes.Add(tnDig);
                        break;
                }
                intPos--;
            }

            for (int i = 1; i < txtNumero.Text.Length; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    switch (j)
                    {
                        case 0: // Valor
                            tnVal = new TreeNode(vNum_Base[intPos].strLabel + " [(" + vNum_Base[intPos].intBase.ToString() + "), " +
                                vNum_Base[intPos].strOperacion + "]");
                            break;
                        case 1: // Digito
                            tnDig = new TreeNode(vNum_Base[intPos].strLabel + " [(" + vNum_Base[intPos].intBase.ToString() + "), " +
                                vNum_Base[intPos].strOperacion + "]");
                            tnDig.Nodes.Add(tnVal);
                            break;
                        case 2: // Numero
                            tnRaiz = new TreeNode(vNum_Base[intPos].strLabel + " [(" + vNum_Base[intPos].intBase.ToString() + "), " +
                                vNum_Base[intPos].strOperacion + "]");
                            tnRaiz.Nodes.Add(tnNum);
                            tnRaiz.Nodes.Add(tnDig);
                            break;
                    }
                    intPos--;
                }
                tnNum = tnRaiz;
            }
            tnNum_b.Nodes.Add(tnNum);
        }
        private void mVoidDerecha()
        {
            tnCBase = new TreeNode(vNum_Base[intRow - 2].strLabel + " (" + vNum_Base[intRow - 2].intBase + ")");
            TreeNode tnBase = new TreeNode(vNum_Base[intRow - 1].strLabel + " (" + vNum_Base[intRow - 1].intBase + ")");
            tnCBase.Nodes.Add(tnBase);
            this.tnNum_b.Nodes.Add(tnCBase);
        }
        private int mIntDigito(char dig)
        {
            switch (dig)
            {
                case '0':
                    return 0;
                case '1':
                    return 1;
                case '2':
                    if (intBase == 2)
                        return -1;
                    else
                        return 2;
                case '3':
                    if (intBase == 2)
                        return -1;
                    else
                        return 3;
                case '4':
                    if (intBase == 2)
                        return -1;
                    else
                        return 4;
                case '5':
                    if (intBase == 2)
                        return -1;
                    else
                        return 5;
                case '6':
                    if (intBase == 2)
                        return -1;
                    else
                        return 6;
                case '7':
                    if (intBase == 2)
                        return -1;
                    else
                        return 7;
                case '8':
                    if(intBase == 8 || intBase == 2)
                        return -1;
                    else
                        return 8;
                case '9':
                    if (intBase == 8 || intBase == 2)
                        return -1;
                    else
                        return 9;
                case 'a': case 'A':
                    if (intBase == 8 || intBase == 10 || intBase == 2)
                        return -1;
                    else
                        return 10;
                case 'b': case 'B':
                    if (intBase == 8 || intBase == 10 || intBase == 2)
                        return -1;
                    else
                        return 11;
                case 'c': case 'C':
                    if (intBase == 8 || intBase == 10 || intBase == 2)
                        return -1;
                    else
                        return 12;
                case 'd': case 'D':
                    if (intBase == 8 || intBase == 10 || intBase == 2)
                        return -1;
                    else
                        return 13;
                case 'e': case 'E':
                    if (intBase == 8 || intBase == 10 || intBase == 2)
                        return -1;
                    else
                        return 14;
                case 'f': case 'F':
                    if (intBase == 8 || intBase == 10 || intBase == 2)
                        return -1;
                    else
                        return 15;
                default:
                    return -1;
            }
        }
    }
}
